/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* rtos_user.h
*
* All of the thread setup and anything a user needs to setup a thread
*
******************************************************************************/
#ifndef __RTOS_USER_H
#define __RTOS_USER_H

#include "msp.h"
#include "os.h"
#include "Libraries/Display.h"
#include "Libraries/PLL.h"
#include "Libraries/I2C.h"
#include "Libraries/Timer.h"
#include "Libraries/ADC.h"

// Include Thread files
#include "Threads/CommunicationThread.h"
#include "Threads/MotorThread.h"
#include "Threads/DirectionThread.h"
#include "Threads/Scheduled.h"

void OS_Init(void);

int OS_AddThreads(void(*task0)(void),
                    void(*task1)(void),
                  void(*task2)(void));
void OS_Launch(uint32_t theTimeSlice);
void GPIO_init(void);

void CommunicationThread(void);
void MotorThread(void);
void DirectionThread(void);
int os_main(void);

#endif
