/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* rtos_user.c
*
******************************************************************************/
#include "rtos_user.h"

void CommunicationThread(void){
    for(;;)
    {
        Communication_Main();
    }
}

void MotorThread(void){
    for(;;)
    {
        Motor_Main();
    }
}

void DirectionThread(void){
    for(;;)
    {
        Direction_Main();
    }
}

int os_main(void){
    OS_Init();           // initialize, disable interrupts

    // Library initializations
    PLL_Init();

    Timer0Init();

    ADC_Init();

    Init_Scheduled();

    Init_Motor();
    Init_Direction();
    Init_Communication();

    // Runs in reverse order
    OS_AddThreads(&CommunicationThread, &DirectionThread, &MotorThread);

    OS_Launch(SysCountReloadValue); // doesn't return, interrupts enabled in here

    return 0;             // this never executes
}
