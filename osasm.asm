;******************************************************************************
;
; CPTR480
; Final Project
; Seth Ballance, JD Priddy
; 6/13/18
;
; osasm.asm
;
; This file contains low level assembly functions that you cannot do in
; straight c such as changing the program counter and all of that low level stuff
;
;******************************************************************************
        .text
        .thumb
        .align 2

        .import RunPt            ; currently running thread
        .global  OS_DisableInterrupts
        .global  OS_EnableInterrupts
        .global  StartOS
        .global  SysTick_Handler
        .global  SVC_Handler
		.global	 StartCritical
		.global	 EndCritical
		.global  Scheduler
		.global  OS_Suspend
PtRun	.field RunPt,32

OS_DisableInterrupts:
        CPSID   I
        BX      LR


OS_EnableInterrupts: .asmfunc
        CPSIE   I
        BX      LR
        .endasmfunc

;*********** StartCritical ************************
; make a copy of previous I bit, disable interrupts
; inputs:  none
; outputs: previous I bit
StartCritical: .asmfunc
        MRS    R0, PRIMASK  ; save old status
        CPSID  I            ; mask all (except faults)
        BX     LR
        .endasmfunc

;*********** EndCritical ************************
; using the copy of previous I bit, restore I bit to previous value
; inputs:  previous I bit
; outputs: none
EndCritical: .asmfunc
        MSR    PRIMASK, R0
        BX     LR
		.endasmfunc

SysTick_Handler: .asmfunc                ; 1) Saves R0-R3,R12,LR,PC,PSR
	mrs r3, primask
    cpsid i
    push {R4, R5, R6, R7, R8, R9, R10, R11, LR}
    mov r0, sp
    mov r4, r3
    BL Scheduler
    mov sp, r0
    movs lr, #0
    msr primask, r4
    pop {R4, R5, R6, R7, R8, R9, R10, R11, PC}
	.endasmfunc

SVC_Handler: .asmfunc                ; 1) Saves R0-R3,R12,LR,PC,PSR
	B SysTick_Handler
	.endasmfunc

OS_Suspend: .asmfunc
    ; SVC interrupt because the normal one takes like 100 clock cycles for whatever reason
	SVC #0
	BX		LR
	.endasmfunc

    .align
    .end
