/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* os.c
*
******************************************************************************/
#include "os.h"

tcbType tcbs[NUMTHREADS] = {};
uint32_t Stacks[NUMTHREADS][STACKSIZE];
ScheduledThread ScheduledThreads[NUMSCHEDULEDTHREADS];
tcbType mainThread = {
      .sp = 0,
      .next = &mainThread,
      .name = "Main Thread"
};
tcbType *RunPt = &mainThread;


// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O & SysTick
// input:  none
// output: none
void OS_Init(void){
    OS_DisableInterrupts();
    SysTick->CTRL = 0;					// disable SysTick during setup
    SysTick->VAL = 0x000000;		    // clear SysTick counter register. any value will do
    SysTick->CTRL = 0x07;				// enable SysTick  select system clock
                                                            //    enable SysTick interrupt, auto reload

    NVIC_SYS_PRI3_R =(NVIC_SYS_PRI3_R&0x00FFFFFF)|NVIC_PRI3_INT15_M; // priority 7
    OS_EnableInterrupts();
}

void * SetInitialStack(void(*fn)(void), uint32_t * stackArray){
    void * stackPointer = &stackArray[STACKSIZE-17];
    const uint32_t r0 = 0;
    const uint32_t r1 = 0;
    const uint32_t r2 = 0;
    const uint32_t r3 = 0;
    const uint32_t r12 = 0;
    const uint32_t lr = (uint32_t)-1;
    uint32_t pc = (uint32_t)fn;
    const uint32_t mpsr = 0x01000000UL;
    const uint32_t r4 = 0;
    const uint32_t r5 = 0;
    const uint32_t r6 = 0;
    const uint32_t r7 = 0;
    const uint32_t r8 = 0;
    const uint32_t r9 = 0;
    const uint32_t r10 = 0;
    const uint32_t fp = 0;
    const uint32_t int_pc = (uint32_t)-7;
    uint32_t * stack = (uint32_t *)(stackPointer);
    uint32_t init_list[] = {
       r4, r5, r6, r7, r8, r9, r10, fp, int_pc, r0, r1, r2, r3, r12, lr, pc, mpsr};
    memcpy(stack, init_list, sizeof(init_list));
    return stackPointer;
}

//******** OS_AddThread ***************
// add three foregound threads to the scheduler
// Inputs: three pointers to a void/void foreground tasks
// Outputs: 1 if successful, 0 if this thread can not be added
int OS_AddThreads(void(*task0)(void),
                 void(*task1)(void),
                 void(*task2)(void)){ int32_t status;
  status = StartCritical();

  void(*fns[])() = {
                    task0,
                    task1,
                    task2
  };

  const char * names[] = {
                    "task0",
                    "task1",
                    "task2"
  };
  int i;
  for (i = 0; i < NUMTHREADS; i++)
  {
      tcbs[i].next = mainThread.next;
      mainThread.next = &tcbs[i];
      tcbs[i].sp = SetInitialStack(fns[i], Stacks[i]);
      tcbs[i].name = names[i];
  }
  EndCritical(status);
  return 1;               // successful
}

void* Scheduler(void* sp) {
    RunPt->sp = sp;
    ScheduledThread * s = ScheduledThreads;
    uint32_t time;
    while(s)
    {
        time = getElapsedTimeInMs();
        if (time - s->lastRun > s->timing)
        {
        	tcbType * saved = RunPt;
        	RunPt = 0;
            s->fun_ptr();
            s->lastRun = time;
            RunPt = saved;
        }
        s = s->next;
    }

    RunPt = RunPt->next;    // run next thread not blocked
    while(RunPt->blocked) { // skip if blocked
        RunPt = RunPt->next;
    }
    return RunPt->sp;
}

void QueueInitialize(Queue* q, void * memory, char size, uint32_t elemSize)
{
    q->head = memory;
    q->size = size;
    q->nextSpace = 0;
    q->elementSize = elemSize;
    q->usedSpace = 0;
    q->s = 1;
}

bool QueueInsertElement(Queue* q, void* element)
{
	if (QueueFull(q))
		return false;

    OS_Wait(&q->s);

    memcpy((void*)((char *)q->head + q->nextSpace * q->elementSize), element, q->elementSize);

    q->nextSpace = (q->nextSpace + 1) % q->size;

    q->usedSpace++;

    OS_Signal(&q->s);

    return true;
}

bool QueueGetElement(Queue* q, void* container)
{
	if (QueueEmpty(q))
		return false;

    OS_Wait(&q->s);

    char place;

    if (q->nextSpace >= q->usedSpace)
        place = (q->nextSpace - q->usedSpace) % q->size;
    else
        place = (q->size - q->nextSpace - q->usedSpace) % q->size;

    memcpy(container, (void *)((char *)q->head + (place * q->elementSize)), q->elementSize);

    q->usedSpace--;

    OS_Signal(&q->s);

    return true;
}

void QueueInsertElementB(Queue* q, void* element)
{
	while(!QueueInsertElement(q, element))
        OS_Suspend();
}

void QueueGetElementB(Queue* q, void* container)
{
    while(!QueueGetElement(q, container))
        OS_Suspend();
}

bool QueueEmpty(Queue* q)
{
    return q->usedSpace == 0;
}

bool QueueFull(Queue* q)
{
    return q->usedSpace == q->size;
}

void QueueDrain(Queue* q)
{
	q->usedSpace = 0;
}

///******** OS_Launch ***************
// start the scheduler, enable interrupts
// Inputs: number of system clock cycles for each time slice
//         (maximum of 24 bits)
// Outputs: none (does not return)
void OS_Launch(uint32_t SysCountValue){
	SysTick->LOAD = SysCountValue;		// counter reload value
	SysTick->CTRL = 0x07;               // enable SysTick  select system clock
	//    enable SysTick interrupt, auto reload
	OS_Suspend();
}

void OS_Signal(Semaphore * s){
    tcbType * pt;
    OS_DisableInterrupts();
    (*s) = (*s) + 1;
    if((*s) <= 0 && RunPt){
        pt = RunPt->next;   // search for one blocked on this
        while(pt->blocked != s){
            pt = pt->next;
        }
        pt->blocked = 0;    // wakeup this one
    }
    OS_EnableInterrupts();
}

void OS_Wait(Semaphore * s){
    OS_DisableInterrupts();
    (*s) = (*s) - 1;
    if((*s) < 0 && RunPt > 0){
        RunPt->blocked = s; // reason it is blocked
        OS_EnableInterrupts();
        OS_Suspend();       // run thread switcher
    }
    OS_EnableInterrupts();
}
