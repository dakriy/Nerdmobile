/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* os.h
*
* This is all of the operating system functionality and thread synchronization
* support.
*
******************************************************************************/
#ifndef __OS_H
#define __OS_H

#include "msp.h"
#include "Libraries/Timer.h"
#include <stdint.h>
#include <stdbool.h>

 #define MAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

 #define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

                                                    // Sys. Handlers 12 to 15 Priority
#define NVIC_SYS_PRI3_R         (*((volatile uint32_t *)0xE000ED20))

// function definitions in osasm.asm
void OS_DisableInterrupts(void); // Disable interrupts
void OS_EnableInterrupts(void);  // Enable interrupts
int32_t StartCritical(void);
void EndCritical(int32_t primask);
void OS_Suspend(void);
void StartOS(void);

// This should equate to about 1 ms switching time
#define SysCountReloadValue  0x1D4C0    // thread switch time in system time units

#define NUMTHREADS  3        // maximum number of threads
#define NUMSCHEDULEDTHREADS 5
#define STACKSIZE   4096      // number of 32-bit words in stack

typedef volatile int32_t Semaphore;

struct tcb{
  void *sp;       // pointer to stack (valid for threads not running
  const char * name;
  struct tcb *next;  // linked-list pointer
  Semaphore * blocked;
};
typedef struct tcb tcbType;

extern tcbType tcbs[NUMTHREADS];
extern tcbType mainThread;
extern tcbType *RunPt;
extern uint32_t Stacks[NUMTHREADS][STACKSIZE];

struct ScheduledThreadStruct {
    // Function pointer
    void (*fun_ptr)(void);

    // How fast to run in ms
    uint32_t timing;

    // Last run elapsed ms
    uint32_t lastRun;

    struct ScheduledThreadStruct *next;  // linked-list pointer
};

typedef struct Queue {
	// Constant after initialization
    void* head; // Memory block
    uint32_t elementSize; // Bytes per element
    char size; // Number of elements

    // Subject to change
    char nextSpace; // Next free slot (if free)
    char usedSpace; // Number of used up elements
    Semaphore s;	// Semaphore for multi-threaded protection
} Queue;

void QueueInitialize(Queue*, void *, char, uint32_t);

/*
 * Inserts an element into a queue.
 * This function is blocking and will wait until there is room in the queue
 * before it returns.
 */
void QueueInsertElementB(Queue*, void*);

/*
 * Inserts an element into a queue if it is not full.
 * If the queue is full, this function will return false.
 */
bool QueueInsertElement(Queue*, void*);

/*
 * Gets an element from a queue. This function is blocking and will block until
 * there is an element in the queue to grab.
 */
void QueueGetElementB(Queue*, void*);

/*
 * Gets an element from a queue and returns true if it is not empty.
 * If the queue is empty, it will return false.
 */
bool QueueGetElement(Queue*, void*);

/*
 * Checks a queue to see if it is empty
 */
bool QueueEmpty(Queue*);

/*
 * Checks a queue to see if it is full
 */
bool QueueFull(Queue*);

/*
 * Empties a queue
 */
void QueueDrain(Queue*);

typedef struct ScheduledThreadStruct ScheduledThread;

extern ScheduledThread ScheduledThreads[NUMSCHEDULEDTHREADS];

void OS_Init(void);
void * SetInitialStack(void(*task0)(void), uint32_t *);
int OS_AddThreads(void(*task0)(void),
                 void(*task1)(void),
                 void(*task2)(void));
void* Scheduler(void*);
void OS_Launch(uint32_t SysCountValue);
void OS_Signal(Semaphore * s);
void OS_Wait(Semaphore * s);

#endif
