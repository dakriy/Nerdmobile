/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* Scheduled.c
*
******************************************************************************/
#include "Scheduled.h"

void Init_Scheduled(void)
{
    // Setup CheckDistance to be run every 5 ms
    ScheduledThreads[0].fun_ptr = FlashLED;
    ScheduledThreads[0].lastRun = 0;
    ScheduledThreads[0].timing = 5;
    ScheduledThreads[0].next = 0;
}

void FlashLED(void)
{
	GPIOF->DATA ^= BIT0;
}
