/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* CommunicationThread.c
*
******************************************************************************/
#include "CommunicationThread.h"

void Init_Communication(void)
{
    QueueInitialize(&displayTopQueue, displayTopTextPointers, OUTPUT_LENGTH, sizeof(char*));
    QueueInitialize(&displayBottomQueue, displayBottomTextPointers, OUTPUT_LENGTH, sizeof(char*));
    QueueInitialize(&serialOutputQueue, serialOutputTextPointers, OUTPUT_LENGTH, sizeof(char*));
//    QueueInitialize(&serialInputQueue, serialInputTextPointers, OUTPUT_LENGTH, sizeof(char*));

    Serial_Init();
    Display_Init();
    //Display_Init();

    I2C_Init();

    initDistanceSensor();
}

volatile bool stoppedOnSensor = false;
volatile uint32_t lastDistanceRead = 0;
volatile uint32_t lastLCDWrite = 0;

void Communication_Main(void)
{
    /*uint32_t t = getElapsedTimeInMs();
    if(t - lastDistanceRead > 250)
    {
        checkDistance();
        lastDistanceRead = t;
    }
    if(t - lastLCDWrite > 100)
    {
        Output();
        lastLCDWrite = t;
    }*/
    OS_Suspend();
}

void Output(void)
{
    OutputSerial();
    OutputDisplay();
}

void OutputSerial(void)
{
    char* newSerialString;
    QueueGetElement(&serialOutputQueue, &newSerialString);
    Serial_OutString(newSerialString);
}

void OutputDisplay(void)
{
    char top[17];
    char bot[17];
    int i;
    int16_t r = getRightSpeed();
    int16_t l = getLeftSpeed();

    // 17 because I want the null terminator
    for(i = 0; i < 16; i++)
    {
        top[i] = bot[i] = ' ';
    }
    top[16] = bot[16] = 0;
    top[0] = 'R';
    top[1] = ':';
    top[2] = ' ';
    int index = 3;
    if(r < 0)
    {
        top[index++] = '-';
        r = -r;
    }
    // 12345
    // (12345 % 10000) / 1000 = 2.345 (integer divison will truncate the decimal)
    for(i = 100000; i >= 10; i /= 10, index++)
    {
        top[index] = '0' + ((r % i) / (i / 10));
    }

    bot[0] = 'L';
    bot[1] = ':';
    bot[2] = ' ';
    index = 3;
    if(l < 0)
    {
        bot[index++] = '-';
        l = -l;
    }
    for(i = 100000; i >= 10; i /= 10, index++)
    {
        bot[index] = '0' + ((l % i) / (i / 10));
    }

    if (position < directionsSize)
    {
        top[15] = (char)Path[position];
    }
    if (position < directionsSize - 1)
    {
        bot[15] = (char)Path[position + 1];
    }
    Display_ClearLCD_Screen();
    Display_OutChar(SCREEN_CTRL_CHAR);
    Display_OutChar(SCREENPOS(0));
    Display_OutString(top);
    Display_OutChar(SCREEN_CTRL_CHAR);
    Display_OutChar(SCREENPOS(64));
    Display_OutString(bot);
}

//void Input(void)
//{
//    uint8_t serialStringLength = 20;
//    char serialInput[serialStringLength];
//    Serial_InString(&serialInput, serialStringLength);
//
//    QueueInsertElement(&serialInputQueue, &serialInput);
//}

//---------------------Serial_AddOutString---------------------
// Add a string to the serial output queue
// Input: string
// Output: none
// Use: Serial_AddOutString("hello world");
void Serial_AddOutString(char * string)
{
    char *s = string;
    QueueInsertElement(&serialOutputQueue, &s);
}

//---------------------Serial_AddOutString---------------------
// Add a string to the serial output queue
// Input: 2 strings
// Output: none
// Use: Display_AddOutStrings("hello world", "foo bar");

char *topHolder;
char *bottomHolder;
void Display_AddOutStrings(char * topString, char * bottomString)
{
    topHolder = topString;
    bottomHolder = bottomString;
    QueueInsertElement(&displayTopQueue, &topHolder);
    QueueInsertElement(&displayBottomQueue, &bottomHolder);
}

char checkDistance()
{
    char dist = getRangeMeasurement();
    // Closer than 7 cm?
    if (dist < 70)
    {
        // Put the motors into indefinite hold for now.
        MotorCommand(Hold);
        stoppedOnSensor = true;
    }
    else {
        if (stoppedOnSensor)
        {
            MotorCommand(Resume);
            stoppedOnSensor = false;
        }
    }
    return dist;
}
