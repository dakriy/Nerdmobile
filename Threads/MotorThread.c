/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* MotorThread.c
*
******************************************************************************/
#include "MotorThread.h"
// In timer counts
int16_t rightWheelTarget = 0;
// In timer counts
int16_t leftWheelTarget = 0;

int16_t rightCurrent = 0;
int16_t leftCurrent = 0;

Queue leftMotorQueue;
Queue rightMotorQueue;

MotorSpeed leftMotorToAddMem[MOTOR_QUEUE_SIZE] = {0};
MotorSpeed rightMotorToAddMem[MOTOR_QUEUE_SIZE] = {0};

bool MOTORstopped = false;
bool MOTORinstant = false;

void Init_Motor(void)
{
    // Initially turn motors off
    PWM1A_Init(PERIOD, MOTOR_OFF);
    PWM0B_Init(PERIOD, MOTOR_OFF);

    // Initialize the queue
    QueueInitialize(&rightMotorQueue, rightMotorToAddMem, MOTOR_QUEUE_SIZE, sizeof(MotorSpeed));
    QueueInitialize(&leftMotorQueue, leftMotorToAddMem, MOTOR_QUEUE_SIZE, sizeof(MotorSpeed));
}

void Motor_Main(void)
{
    MotorSpeed rightModifier;
    MotorSpeed leftModifier;

    // Find out what needs to change
    while (QueueGetElement(&leftMotorQueue, &leftModifier))
    {
        if (leftModifier.absolute)
            leftWheelTarget = leftModifier.value;
        else
            leftWheelTarget += leftModifier.value;
    }
    while (QueueGetElement(&rightMotorQueue, &rightModifier))
    {
        if (rightModifier.absolute)
            rightWheelTarget = rightModifier.value;
        else
            rightWheelTarget += rightModifier.value;
    }

    // Update motor speed
    if (MOTORinstant)
    {
        rightCurrent = rightWheelTarget;
        leftCurrent = leftWheelTarget;
    }
    else
    {
        // fourth of the difference per loop?
        rightCurrent += (rightWheelTarget - rightCurrent) / DELTA_JUMP_PER_LOOP;
        leftCurrent += (leftWheelTarget - leftCurrent) / DELTA_JUMP_PER_LOOP;
    }


    // Update motors
    if (!MOTORstopped)
    {
        if (leftCurrent < -32767)
            leftCurrent = -32767;
        if (rightCurrent < -MOTOR_OFF)
            rightCurrent = -MOTOR_OFF;
        if (leftCurrent > MOTOR_OFF)
            leftCurrent = MOTOR_OFF; // Max of a int16_t
        if(rightCurrent > 32767 - MOTOR_OFF)
            rightCurrent = (32767 - MOTOR_OFF); // Max of a int16_t

        // left wheel - for forward
        PWM1A_Duty(MOTOR_OFF - leftCurrent);

        // right wheel + for forward
        PWM0B_Duty(MOTOR_OFF + rightCurrent);
    }
    else {
        PWM1A_Duty(MOTOR_OFF);
        PWM0B_Duty(MOTOR_OFF);
    }

    // Done this round
    OS_Suspend();
}


void MotorCommand(Command c)
{
	switch (c) {
	case Stop: // Stops the vehicle clears target and current
		leftCurrent = 0;
		rightCurrent = 0;
		QueueDrain(&leftMotorQueue);
		QueueDrain(&rightMotorQueue);
		PWM1A_Duty(MOTOR_OFF);
		PWM0B_Duty(MOTOR_OFF);
	case Clear: // Clears the Target value to 0
		rightWheelTarget = 0;
		leftWheelTarget = 0;
		break;
	case Hold: // Temporarily pauses any movement
		MOTORstopped = true;
		PWM1A_Duty(MOTOR_OFF);
		PWM0B_Duty(MOTOR_OFF);
		break;
	case InstantOn: // Turns on instant speed changing
		MOTORinstant = true;
		break;
	case InstantOff: // Smoother accelerations
		MOTORinstant = false;
		break;
	case Equalize: {// Equalizes the motor speeds to average (not instant)
		MotorSpeed targetVal = {
				.absolute = true,
				.value = (rightWheelTarget + leftWheelTarget) * 0.5
		};
		QueueDrain(&leftMotorQueue);
		QueueDrain(&rightMotorQueue);
		QueueInsertElement(&leftMotorQueue, &targetVal);
		QueueInsertElement(&rightMotorQueue, &targetVal);
		break;
	}
	case Resume: // Resumes the car from a temporary stop.
		MOTORstopped = false;
	default:
		break;
	}
	return;
}

int16_t getRightSpeed(void)
{
	return (rightCurrent);
}

int16_t getLeftSpeed(void)
{
	return (leftCurrent);
}
