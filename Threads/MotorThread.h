/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* MotorThread.h
*
* This contains all of the functions and commands for driving the motors
*
******************************************************************************/
#ifndef MOTORTHREAD_H_
#define MOTORTHREAD_H_

#include "Libraries/PWM.h"
#include "os.h"

// Right wheel is PWM0B with yellow on PF1 add for forward
// Left wheel is PWM1A with blue on PF2 subtract for forward

#define MOTOR_OFF 2813
#define PERIOD 37500
#define DELTA_JUMP_PER_LOOP 4.f
#define MOTOR_QUEUE_SIZE 5

typedef struct MotorSpeed {
    int16_t value;
	bool absolute;
} MotorSpeed;

extern Queue leftMotorQueue;
extern Queue rightMotorQueue;

extern MotorSpeed leftMotorToAddMem[MOTOR_QUEUE_SIZE];
extern MotorSpeed rightMotorToAddMem[MOTOR_QUEUE_SIZE];

extern int16_t rightWheelTarget;
extern int16_t leftWheelTarget;

extern int16_t rightCurrent;
extern int16_t leftCurrent;

extern bool MOTORstopped;
extern bool MOTORinstant;

typedef enum Command {
	Clear,		// Clears the Target value to 0
	Stop,		// Stops the vehicle clears target and current
	Hold,		// Temporarily pauses any movement
	Resume,		// Resumes the car from a temporary stop.
	Equalize,	// Equalizes the motor speeds (not instant)
	InstantOn,	// Turns on instant speed changing
	InstantOff	// Smoother accelerations
} Command;

void Init_Motor(void);

void Motor_Main(void);

void MotorCommand(Command);

int16_t getRightSpeed(void);
int16_t getLeftSpeed(void);


#endif /* MOTORTHREAD_H_ */
