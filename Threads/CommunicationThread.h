/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* CommunicationThread.h
*
* Thread for doing long UART and I2C communications
*
******************************************************************************/
#ifndef COMMUNICATIONTHREAD_H_
#define COMMUNICATIONTHREAD_H_

#include "../os.h"
#include "Libraries/Serial.h"
#include "Libraries/Display.h"
#include "Libraries/DistanceSensor.h"
#include "MotorThread.h"
#include "DirectionThread.h"

#define OUTPUT_LENGTH 10

Queue displayTopQueue;
Queue displayBottomQueue;
Queue serialOutputQueue;
Queue serialInputQueue;

char* displayTopTextPointers[OUTPUT_LENGTH];
char* displayBottomTextPointers[OUTPUT_LENGTH];
char* serialOutputTextPointers[OUTPUT_LENGTH];
//char* serialInputTextPointers[OUTPUT_LENGTH];

void Init_Communication(void);

void Communication_Main(void);

void Output(void);

void OutputSerial(void);

void OutputDisplay(void);

void Input(void);

void Serial_AddOutString(char * string);

void Display_AddOutStrings(char * topString, char * bottomString);

char checkDistance();


#endif /* COMMUNICATIONTHREAD_H_ */
