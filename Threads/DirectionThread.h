/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* DirectionThread.h
*
* This contains the main path finding algorithm for following the lines.
* To set the path, just change the path variable with what directions you
* want to give it.
*
******************************************************************************/
#ifndef DIRECTIONTHREAD_H_
#define DIRECTIONTHREAD_H_
#include "os.h"
#include "Libraries/ADC.h"
#include "Libraries/Timer.h"
#include "MotorThread.h"

// In percent
// Keep it slow for now so that you can catch it if it runs off...
#define START_SPEED_L       33
#define START_SPEED_R       61

// These values are calibrated so that the robot spins about the points of contact on the wheels.
// So in other words on the point in between the wheels.
#define R_TURN_SPEED_L      30
#define R_TURN_SPEED_R      -30

// These values are calibrated so that the robot spins about the points of contact on the wheels.
// So in other words on the point in between the wheels.
#define L_TURN_SPEED_L    -100
#define L_TURN_SPEED_R    70

#define ERROR_MARGIN   400

/*
 * |-----------|
 * | 5 | 7 | 6 |
 * |-----------|
 * | 3 | 8 | 4 |
 * |-----------|
 * | 0 | 1 | 2 |
 * |-----------|
 * PANEL FACING DOWN
 */
extern uint16_t ADCVals[9];
extern const uint16_t ADCLowBreak[9];
extern const uint16_t ADCHighBreak[9];

extern volatile bool destinationReached;
extern volatile bool finishedTurn;
extern volatile bool turning;
extern volatile bool start;
extern volatile bool incCorrection;
extern volatile bool processedIntersection;
extern volatile uint32_t lastIntersectionCrossing;

extern volatile uint16_t position;

typedef enum Directions {
	Left = 'L',
	Right = 'R',
	Straight = 'S'
} Directions;

typedef enum Positions {
    TopRight = 0,
    TopTop = 1,
    TopLeft = 2,
    MiddleRight = 3,
    MiddleMiddle = 8,
    MiddleLeft = 4,
    BottomRight = 5,
    BottomBottom = 7,
    BottomLeft = 6
} Positions;

extern Directions Path[];

extern const uint16_t directionsSize;

void Init_Direction(void);

void Direction_Main(void);

bool CheckADC(Positions);

bool CheckADCOff(Positions);

uint16_t ADCVal(Positions);

bool OnIntersection();

bool Indeterminate(Positions);

bool OffTrack();

bool topOnIntersection();

#endif /* DIRECTIONTHREAD_H_ */
