/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* Scheduled.h
*
* This file and it's .c compliment contain the framework for scheduled threads
* and all of that good stuff
*
******************************************************************************/
#ifndef SCHEDULED_H_
#define SCHEDULED_H_

#include <stdint.h>
#include "os.h"

void Init_Scheduled(void);

void FlashLED(void);

#endif /* SCHEDULED_H_ */
