/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* DirectionThread.c
*
******************************************************************************/
#include "DirectionThread.h"



// Rough estimates
const uint16_t ADCLowBreak[9] = {
		450, 450, 450, 450, 450, 450, 450, 400, 300
};

// Rough estimates
const uint16_t ADCHighBreak[9] = {
		2200, 2200, 2200, 2200, 2200, 2200, 2200, 1300, 330
};

volatile bool destinationReached = false;
volatile bool finishedTurn = false;
volatile bool turning = false;
volatile bool start = true;
volatile bool incCorrection = false;

volatile uint16_t position = 0;
volatile bool processedIntersection = false;

Directions Path[] = {
                     Straight, Left, Right, Straight, Straight
};

const uint16_t directionsSize = (sizeof(Path)/sizeof(Directions));
/*
 * |-----------|
 * | 5 | 7 | 6 |
 * |-----------|
 * | 3 | 8 | 4 |
 * |-----------|
 * | 0 | 1 | 2 |
 * |-----------|
 * PANEL FACING DOWN
 * Front
 */
uint16_t ADCVals[9] = {0};

void Init_Direction(void)
{
	// Just gonna poll this in this thread when I need it cause it only takes like 80 uS and I'm only doing it here
	// So not worth it to me to put it into it's own scheduled thread
	PollADC(ADCVals);
	MotorCommand(InstantOn);
}

bool CheckADC(Positions p)
{
    return ADCVals[p] > ADCHighBreak[p];
}

bool CheckADCOff(Positions p)
{
    return ADCVals[p] < ADCLowBreak[p];
}

bool OnIntersection()
{
    return (uint8_t)CheckADC(MiddleRight) +
            (uint8_t)CheckADC(MiddleMiddle) +
            (uint8_t)CheckADC(MiddleLeft) +
            (uint8_t)CheckADC(TopTop) +
            (uint8_t)CheckADC(BottomBottom) == 5 ||
            (uint8_t)CheckADC(TopLeft) +
            (uint8_t)CheckADC(MiddleLeft) +
            (uint8_t)CheckADC(BottomLeft) +
            (uint8_t)CheckADC(MiddleMiddle) +
            (uint8_t)CheckADC(MiddleRight) == 5 ||
            (uint8_t)CheckADC(TopRight) +
            (uint8_t)CheckADC(MiddleRight) +
            (uint8_t)CheckADC(BottomRight) +
            (uint8_t)CheckADC(MiddleMiddle) +
            (uint8_t)CheckADC(MiddleLeft) == 5;
}

bool Indeterminate(Positions p)
{
    return CheckADC(p) && (ADCVals[p] > ADCLowBreak[p]);
}

bool topOnIntersection()
{
    return (uint8_t)CheckADC(TopRight) +
            (uint8_t)CheckADC(TopLeft) +
            (uint8_t)CheckADC(TopTop) == 3;
}

bool OffTrack()
{
    return CheckADCOff(TopRight) &&
            CheckADCOff(TopLeft) &&
            CheckADCOff(TopTop) &&
            CheckADCOff(MiddleMiddle) &&
            CheckADCOff(MiddleLeft) &&
            CheckADCOff(MiddleRight) &&
            CheckADCOff(BottomBottom) &&
            CheckADCOff(BottomRight) &&
            CheckADCOff(BottomLeft);
}

uint16_t ADCVal(Positions p)
{
    return ADCVals[p];
}

volatile uint32_t lastIntersectionCrossing = 0;
volatile bool lastRunWasOff = false;
volatile bool toggleDelay = false;
volatile uint32_t turnToggleTime = 0;

volatile bool startTurn = false;
volatile bool firstTurnRun = false;

void Direction_Main(void)
{
    volatile uint32_t time = getElapsedTimeInMs();
    destinationReached = position >= sizeof(Path)/sizeof(Directions) || destinationReached;
    if (destinationReached)
    {
        MotorCommand(Hold);
        OS_Suspend();
        return;
    }
    bool onIntersection = topOnIntersection();


    MotorSpeed right, left;

    // Start us off by doing a conversion
    PollADC(ADCVals);
//    time = getElapsedTimeInUs();

    // Wait for us to be put onto a track
    while (start)
    {
        PollADC(ADCVals);
        // Check the center column to see if we are on a line
        if (ADCVals[TopTop] > ADCHighBreak[TopTop] ||
                ADCVals[TopLeft] > ADCHighBreak[TopLeft] ||
                ADCVals[TopRight] > ADCVals[TopRight])
            break;
    }

    right.value = 0;
    left.value = 0;
    left.absolute = right.absolute = false;

    if(!onIntersection && (time - lastIntersectionCrossing) > 2000)
    {
        lastIntersectionCrossing = time;
        processedIntersection = false;
    }

    if (toggleDelay)
    {
        // This 1500 ms delay is calibrated to the current motor speeds so that the wheels stop on the black line
        if(getElapsedTimeInMs() - turnToggleTime > 1400)
        {
            MotorCommand(Stop);
            toggleDelay = false;
            turning = firstTurnRun = true;
            return;
        }
    }

    // If finished the turn increment the position counter and return control to the P controller

    if (finishedTurn && processedIntersection)
    {
        position++;
        finishedTurn = false;
    }

    // If we are just starting just start us off at the normal speed.
    if(start)
    {
        left.absolute = right.absolute = true;
        right.value = START_SPEED_R;
        left.value = START_SPEED_L;
        start = false;
    }
    else if (!turning)
    {
        if(OffTrack())
        {
            if (!lastRunWasOff)
                lastRunWasOff = true;
            else
                destinationReached = true;
            return;
        }
        else
            destinationReached = false;

        left.absolute = right.absolute = false;


        // Error Controller:

        // P controller
        // Calculate Error on left
        const float kpl = 0.4f;
        int32_t ekpl;
        if (!CheckADCOff(TopLeft) && !onIntersection)
            ekpl = kpl * ADCVals[TopLeft] - ADCLowBreak[TopLeft];
        else
            ekpl = 0;

        // Calculate Error on Right
        const float kpr = 0.5f;
        int32_t ekpr;
        if(!CheckADCOff(TopRight) && !onIntersection)
            ekpr = kpr * ADCVals[TopRight] - ADCLowBreak[TopRight];
        else
            ekpr = 0;


        // I controller
        /*uint32_t dt = getElapsedTimeInUs() - time;
        const float kir = -0.01f;
        const float kil = -0.01f;
        int32_t ekil = dt * el * kil;
        int32_t ekir = dt * er * kir;*/

        if((ekpr > ERROR_MARGIN))
            left.value = (ekpr) / 500.f;
        else if (ekpr < ERROR_MARGIN - 100)
        {
            left.value = START_SPEED_L;
            left.absolute = true;
        }
        else
            left.value = 0;

        if((ekpl > ERROR_MARGIN))
            right.value = (ekpl) / 500.f;
        else if (ekpl < ERROR_MARGIN - 100)
        {
            right.value = START_SPEED_R;
            right.absolute = true;
        }
        else
            right.value = 0;

        // Look for the cross section on the middle sensors
        if (onIntersection && !processedIntersection)
        {
            // We at an intersection boys!
            turnToggleTime = getElapsedTimeInMs();
            toggleDelay = true;
            processedIntersection = true;
            return;
        }
    }
    else // Turning
    {
        left.absolute = right.absolute = true;
        switch (Path[position]) {
        case Straight: // Sweet we done
            turning = false;
            finishedTurn = true;
            right.value = START_SPEED_R;
            left.value = START_SPEED_L;
            break;
        case Left: // Turn left
            left.value = L_TURN_SPEED_L;
            right.value = L_TURN_SPEED_R;
            if (firstTurnRun)
                startTurn = true;
            break;
        case Right: // Turn right
            left.value = R_TURN_SPEED_L;
            right.value = R_TURN_SPEED_R;
            if(firstTurnRun)
                startTurn = true;
        default:
            break;
        }

        firstTurnRun = false;

        // Wait to line up.
        if ((CheckADC(TopTop) || CheckADC(BottomBottom) || CheckADC(MiddleMiddle)) && !startTurn)
        {
            turning = false;
            MotorCommand(Stop);
            finishedTurn = true;
            left.value = 0;
            right.value = 0;
        }
        // Wait to get off first before line up.
        if(OffTrack()) {
            startTurn = false;
        }
    }

    // Stick our results onto the queue to be processed
    QueueInsertElementB(&leftMotorQueue, &left);
    QueueInsertElementB(&rightMotorQueue, &right);

    // Suspend so that the motor thread can run and update what we calculated
    OS_Suspend();
}
