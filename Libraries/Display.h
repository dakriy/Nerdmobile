/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* Display.h
*
* Functions and support for the LCD Display
*
******************************************************************************/
#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <stdint.h>
#include "msp.h"

#define CR   0x0D
#define LF   0x0A
#define BS   0x08
#define ESC  0x1B
#define SP   0x20
#define DEL  0x7F
#define SCREEN_CTRL_CHAR 0xFE
#define DISPLAY_CLEAR_CMD 0x01
#define SCREENPOS(x) (0x80 + x)

//------------Display_Init------------
// Initialize Display0 for 115,200 baud rate (clock from 16 MHz PIOSC),
// 8 bit word length, no parity bits, one stop bit, FIFOs enabled,
// no interrupts
// Input: none
// Output: none
void Display_Init(void);

//------------Display_InChar------------
// Wait for new serial port input
// Input: none
// Output: ASCII code for key typed
char Display_InChar(void);

//------------Display_OutChar------------
// Output 8-bit to serial port
// Input: letter is an 8-bit ASCII character to be transferred
// Output: none
void Display_OutChar(char data);

//------------Display_OutString------------
// Output String (NULL termination)
// Input: pointer to a NULL-terminated string to be transferred
// Output: none
void Display_OutString(char *pt);

//------------Display_InUDec------------
// InUDec accepts ASCII input in unsigned decimal format
//     and converts to a 32-bit unsigned number
//     valid range is 0 to 4294967295 (2^32-1)
// Input: none
// Output: 32-bit unsigned number
// If you enter a number above 4294967295, it will return an incorrect value
// Backspace will remove last digit typed
uint32_t Display_InUDec(void);

//-----------------------Display_OutUDec-----------------------
// Output a 32-bit number in unsigned decimal format
// Input: 32-bit number to be transferred
// Output: none
// Variable format 1-10 digits with no space before or after
void Display_OutUDec(uint32_t n);

//---------------------Display_InUHex----------------------------------------
// Accepts ASCII input in unsigned hexadecimal (base 16) format
// Input: none
// Output: 32-bit unsigned number
// No '$' or '0x' need be entered, just the 1 to 8 hex digits
// It will convert lower case a-f to uppercase A-F
//     and converts to a 16 bit unsigned number
//     value range is 0 to FFFFFFFF
// If you enter a number above FFFFFFFF, it will return an incorrect value
// Backspace will remove last digit typed
uint32_t Display_InUHex(void);

//--------------------------Display_OutUHex----------------------------
// Output a 32-bit number in unsigned hexadecimal format
// Input: 32-bit number to be transferred
// Output: none
// Variable format 1 to 8 digits with no space before or after
void Display_OutUHex(uint32_t number);

//------------Display_InString------------
// Accepts ASCII characters from the serial port
//    and adds them to a string until <enter> is typed
//    or until max length of the string is reached.
// It echoes each character as it is inputted.
// If a backspace is inputted, the string is modified
//    and the backspace is echoed
// terminates the string with a null character
// uses busy-waiting synchronization on RDRF
// Input: pointer to empty buffer, size of buffer
// Output: Null terminated string
// -- Modified by Agustinus Darmawan + Mingjie Qiu --
void Display_InString(char *bufPt, uint16_t max);

void Display_ClearLCD_Screen(void);

void Display_NewLine(void);

#endif
