/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* PWM.c
*
* This uses PWM0B on PF1 and PWM1A on PF2 to generate PWM.
*
******************************************************************************/

//  DISCLAIMER: COMMENTS MAY NOT BE 100% ACCURATE BUT...

#include "PWM.h"

// period is 16-bit number of PWM clock cycles in one period (3<=period)
// period for PF0 and PF1 must be the same
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2
//                = 120 MHz/2 = 60 MHz (in this example)
// Output on PF2/M0PWM2
void PWM1A_Init(uint16_t period, uint16_t duty) {
    SYSCTL->RCGCPWM |= SYSCTL_RCGCPWM_R0; // 1) activate clock for PWM0
                                          // 2) activate clock for Port F
    SYSCTL->RCGCGPIO |= SYSCTL_RCGCGPIO_R5;
                                          // allow time for clock to stabilize
    while((SYSCTL->PRGPIO & SYSCTL_PRGPIO_R5) == 0){}

    GPIOF->AFSEL |= BIT2;                  // 3) enable alt funct on PF2

    GPIOF->DEN |= BIT2;             //    enable digital I/O on PF2
                                          // 4) configure PF2 as PWM0
    GPIOF->PCTL |= 0x00000600;
    GPIOF->AMSEL &= ~BIT2;          //    disable analog functionality on PF2
                                          // allow time for clock to stabilize
    while((SYSCTL->PRPWM & SYSCTL_PRPWM_R0) == 0){}
    PWM0->CC |= PWM_CC_USEPWM;        // 5) use PWM divider
    PWM0->CC &= ~PWM_CC_PWMDIV_M;     //    clear PWM divider field
    PWM0->CC |= PWM_CC_PWMDIV_64;     //    configure for /64 divider

    PWM0->CTL = 0;                     // 6) re-loading down-counting mode
    PWM0->_1_GENA = (PWM_1_GENA_ACTCMPAD_ONE|PWM_1_GENA_ACTLOAD_ZERO);
    // PF2 goes low on LOAD
    // PF2 goes high on CMPB down
    PWM0->_1_LOAD = period - 1;           // 7) cycles needed to count down to 0
    PWM0->_1_CMPA = duty - 1;             // 8) count value when output rises
    PWM0->_1_CTL |= PWM_1_CTL_ENABLE;     // 9) start PWM0
    PWM0->ENABLE |= PWM_ENABLE_PWM2EN;   // 10) enable PWM1A/PF2 outputs
}

// change duty cycle of PWM1A/PF2
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
void PWM1A_Duty(uint16_t duty){
    PWM0->_1_CMPA = duty - 1;             // 8) count value when output rises
}

// period is 16-bit number of PWM clock cycles in one period (3<=period)
// period for PF0 and PF1 must be the same
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2
//                = 120 MHz/2 = 60 MHz (in this example)
// Output on PF1/M0PWM1
void PWM0B_Init(uint16_t period, uint16_t duty){
  SYSCTL->RCGCPWM |= SYSCTL_RCGCPWM_R0;// 1) activate clock for PWM0
                                        // 2) activate clock for Port F
  SYSCTL->RCGCGPIO |= SYSCTL_RCGCGPIO_R5;
                                        // allow time for clock to stabilize
  while((SYSCTL->PRGPIO & SYSCTL_PRGPIO_R5) == 0){}
  GPIOF->AFSEL |= BIT1;           // 3) enable alt funct on PF1
  GPIOF->DEN |= BIT1;             //    enable digital I/O on PF1
                                        // 4) configure PF1 as PWM0
  GPIOF->PCTL |= 0x00000060;
  GPIOF->AMSEL &= ~BIT1;          //    disable analog functionality on PF1
                                        // allow time for clock to stabilize
  while((SYSCTL->PRPWM & SYSCTL_PRPWM_R0) == 0){}
  PWM0->CC |= PWM_CC_USEPWM;           // 5) use PWM divider
  PWM0->CC &= ~PWM_CC_PWMDIV_M;        //    clear PWM divider field
  PWM0->CC |= PWM_CC_PWMDIV_64;         //    configure for /2 divider
  PWM0->_0_CTL = 0;                     // 6) re-loading down-counting mode
  PWM0->_0_GENB = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO);
  // PF1 goes low on LOAD
  // PF1 goes high on CMPB down
  PWM0->_0_LOAD = period - 1;           // 7) cycles needed to count down to 0
  PWM0->_0_CMPB = duty - 1;             // 8) count value when output rises
  PWM0->_0_CTL |= PWM_0_CTL_ENABLE;     // 9) start PWM0
  PWM0->ENABLE |= PWM_ENABLE_PWM1EN;   // 10) enable PWM0B/PF1 outputs
}

// change duty cycle of PWM0B/PF1
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
void PWM0B_Duty(uint16_t duty){
  PWM0->_0_CMPB = duty - 1;             // 8) count value when output rises
}
