/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* PWM.h
*
******************************************************************************/
#ifndef PWM_H_
#define PWM_H_
#include <stdint.h>
#include "msp.h"
// period is 16-bit number of PWM clock cycles in one period (3<=period)
// period for PF0 and PF1 must be the same
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2
//                = 120 MHz/2 = 60 MHz (in this example)
// Output on PF2/M0PWM1

void PWM1A_Init(uint16_t period, uint16_t duty);

// change duty cycle of PWM1A/PF2
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
void PWM1A_Duty(uint16_t duty);

// period is 16-bit number of PWM clock cycles in one period (3<=period)
// period for PF0 and PF1 must be the same
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2
//                = 120 MHz/2 = 60 MHz (in this example)
// Output on PF1/M0PWM1
void PWM0B_Init(uint16_t period, uint16_t duty);

// change duty cycle of PWM0B/PF1
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
void PWM0B_Duty(uint16_t duty);
#endif
