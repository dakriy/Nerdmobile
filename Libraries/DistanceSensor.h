/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* DistanceSensor.h
*
* Simple interface for the VL6180X distance sensor.
* Uses the included I2C and Timer libraries.
* PINS:
* Distance Sensor Pin | Board Pin              | Function
* SCL                 | SCL - PB2 - I2C0SCL    | I2C clock
* SDA                 | SDA - PB3 - I2C0SDA    | I2C data line
* GPIO0/CE            | PC7                    | Power on line
* VIN                 | 3V3                    | Power to the Sensor
* VDD                 | NONE                   | Internal sensor VDD Does not connect to anything
* GPIO1               | NONE                   | Interrupt pin used to signal when finished with a measurement if interrupts are configured (Not currently in use)
*
******************************************************************************/
#ifndef DISTANCESENSOR_H_
#define DISTANCESENSOR_H_

#include "I2C.h"
#include "Timer.h"

// Slave address specified by the data sheet
#define DISTANCE_SENSOR_SLAVE_ADDRESS 0x29


/*
 * Initializes the distance sensor.
 * Assumes I2C has already been initialized.
 */
void initDistanceSensor(void);


/*
 * Writes a 2 byte register address with
 * the desired byte.
 *
 * Returns 1 on failure, 0 on success
 */
int writeReg(uint16_t address, char byte);

/*
 * Reads a 1 byte register on the device with a 2 byte address.
 *
 * Returns 1 on failure, 0 on success
 */
int readReg(uint16_t address, char* byte);

/*
 * Clears speific bits in an address
 */
int clearBits(uint16_t address, int bitMask);

/*
 * Sets specific bits in an address
 */
int setBits(uint16_t address, int bitMask);

/*
 * Returns the range in mm.
 * Only good to about 100 mm.
 * Could be better if conditions are good
 */
char getRangeMeasurement(void);

/*
 * Returns the ambient light in Lux/count
 */
short getAmbientLight(void);

#endif /* DISTANCESENSOR_H_ */
