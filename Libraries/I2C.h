/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* I2C.h
*
* Simple I2C library. Uses I2C0 and included Timer and PLL libraries.
*
******************************************************************************/

#ifndef I2C_H_
#define I2C_H_

#include "msp.h"
#include "Timer.h"

// I2C Baud rate, configured for 100kHz right now.
#define I2C_CLOCK_SPEED 100000

/*
 * This procedure assumes that PLL and the timer libraries have already been initialized and that the processor is clocked at 120MHz
 */
void I2C_Init(void);

/*
 * Sends one byte to the specified slave address
 *
 * Return 1 on failure, 0 on success
 */
int I2C_SendByte(char slave_address, char byte_to_send);

/*
 * Sends multiple bytes to the specified slave address using repeated start
 *
 * Return 1 on failure, 0 on success
 */
int I2C_SendBytes(char slave_address, char * pointer_to_sequential_data, int length_of_data_array);

/*
 * Requests a byte to be sent from a slave address
 *
 * Return 1 on failure, 0 on success
 */
int I2C_ReceiveByte(char slave_address, char * pointer_to_put_recieved_data_in);

/*
 * Requests length bytes from slave_address and places them into receive_data_structure
 *
 * Return 1 on failure, 0 on success
 */
int I2C_ReceiveBytes(char slave_address, char* receive_data_structure, int length);

#endif /* I2C_H_ */
