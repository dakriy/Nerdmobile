/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* I2C.c
*
* In this file before every
* 'while(I2C0->MCS & I2C_MCS_BUSY);'
* is a line
* 'WaitUs(1000000*.6/I2C_CLOCK_SPEED);'
* This waits 60% of the I2C clock cycle before checking the busy bit.
* This is done because the data sheet specifies that the busy bit
* can take up to 60% of a I2C clock cycle to get set. So, it is necessary
* to always just wait 60% of a clock cycle before checking the busy bit
*
******************************************************************************/
/*
 * In this file before every
 * 'while(I2C0->MCS & I2C_MCS_BUSY);'
 * is a line
 * 'WaitUs(1000000*.6/I2C_CLOCK_SPEED);'
 * This waits 60% of the I2C clock cycle before checking the busy bit.
 * This is done because the data sheet specifies that the busy bit
 * can take up to 60% of a I2C clock cycle to get set. So, it is necessary
 * to always just wait 60% of a clock cycle before checking the busy bit
 */

#include "I2C.h"

void I2C_Init(void)
{

    // Enable I2C0
    SYSCTL->RCGCI2C |= SYSCTL_RCGCI2C_R0;

    // Wait for I2C0 to come online
    while((SYSCTL->PRI2C & SYSCTL_PRI2C_R0) == 0);


    // Enable PORTB Clock
    SYSCTL->RCGCGPIO |= SYSCTL_RCGCGPIO_R1;

    // Wait for clock to stabilize.
    while((SYSCTL->PRGPIO & SYSCTL_PRGPIO_R1) == 0);

    GPIOB->DEN |= BIT2 | BIT3;

    // Enable the function select
    GPIOB->AFSEL |= BIT2 | BIT3;

    // Configure SDA for open drain
    GPIOB->ODR |= BIT3;

    // Select the I2C function for PB2 and PB3
    GPIOB->PCTL |= (0x2 << 8) | (0x2 << 12);

    // Make this device the master
    I2C0->MCR = 0x00000010;

    // Assume system clock is at 120MHz as is in PLL.c
    // Set the clock to 100 kbps
    // TPR = (System Clock / (2 * (SCL_LP + SCL_HP) * SCL_CLK)) - 1
    // TPR = (120 MHz / (2 * (6 + 4) * 100000)) - 1
    // TPR = 59
    I2C0->MTPR = 59;

}

int I2C_SendByte(char address, char data)
{

    // Set slave address and set transmit
    I2C0->MSA = address << 1;

    I2C0->MDR = data;

    // Make sure bus isn't busy
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    // Initiate data transfer
    I2C0->MCS = I2C_MCS_STOP | I2C_MCS_START | I2C_MCS_RUN;

    // Wait for transfer to complete
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    // Return 1 on failure, 0 on success
    return (I2C0->MCS & I2C_MCS_ERROR) >> 1;
}

int I2C_SendBytes(char address, char * data, int length)
{
    int i;
    // Nothing to send.
    if (length < 1)
        return 0;

    if(length == 1)
        return I2C_SendByte(address, *data);

    // Set slave address and set transmit
    I2C0->MSA = address << 1;


    I2C0->MDR = data[0];

    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    // Make sure bus isn't busy
    while(I2C0->MCS & I2C_MCS_BUSY);

    I2C0->MCS = I2C_MCS_START | I2C_MCS_RUN;

    for (i = 1; i < length; i++)
    {

        // Wait for any transfers to finish
        WaitUs(1000000*.6/I2C_CLOCK_SPEED);
        while(I2C0->MCS & I2C_MCS_BUSY);

        if(I2C0->MCS & I2C_MCS_ERROR)
        {
            if(I2C0->MCS & I2C_MCS_ARBLST)
                return 1;
            I2C0->MCS = I2C_MCS_STOP;
            return 1;
        }

        I2C0->MDR = data[i];

        if(i < length - 1)
            I2C0->MCS = I2C_MCS_RUN;
    }

    I2C0->MCS = I2C_MCS_RUN | I2C_MCS_STOP;

    // Wait for last transfer to finish
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    if(I2C0->MCS & I2C_MCS_ERROR)
        return 1;

    return 0;
}

int I2C_ReceiveByte(char address, char * buffer)
{
    // Set slave address to 0x29 and set the next operation to be a receive
    I2C0->MSA = (address << 1) | 0x1;

    // Make sure bus isn't busy
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    I2C0->MCS = I2C_MCS_STOP | I2C_MCS_START | I2C_MCS_RUN;

    // Wait for transfer to complete
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    // Error so abort.
    if (I2C0->MCS & I2C_MCS_ERROR)
        return 1;
    // Write the received data into the buffer
    *buffer = I2C0->MDR;

    return 0;
}

int I2C_ReceiveBytes(char address, char * buffer, int bytesToRead)
{
    int i;

    // Nothing to receive.
    if(bytesToRead < 1)
        return 0;

    if (bytesToRead == 1)
        return I2C_ReceiveByte(address, buffer);

    // Set slave address to 0x29 and set the next operation to be a receive
    I2C0->MSA = (address << 1) | 0x1;

    // Make sure bus isn't busy
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    I2C0->MCS = I2C_MCS_RUN | I2C_MCS_START | I2C_MCS_ACK;

    for (i = 0; i < bytesToRead - 1; i++)
    {
        // Make sure bus isn't busy/wait for any transfers to finish
        WaitUs(1000000*.6/I2C_CLOCK_SPEED);
        while(I2C0->MCS & I2C_MCS_BUSY);

        if(I2C0->MCS & I2C_MCS_ERROR)
        {
            if(I2C0->MCS & I2C_MCS_ARBLST)
                return 1;
            I2C0->MCS = I2C_MCS_STOP;
            return 1;
        }

        buffer[i] = I2C0->MDR;

        if (i < bytesToRead - 2)
            I2C0->MCS = I2C_MCS_DATACK | I2C_MCS_RUN;
    }

    I2C0->MCS = I2C_MCS_RUN | I2C_MCS_STOP;

    // Make sure bus isn't busy
    WaitUs(1000000*.6/I2C_CLOCK_SPEED);
    while(I2C0->MCS & I2C_MCS_BUSY);

    if(I2C0->MCS & I2C_MCS_ERROR)
        return 1;

    buffer[bytesToRead - 1] = I2C0->MDR;

    return 0;
}
