/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* PLL.c
*
******************************************************************************/
#include <stdint.h>
#include "PLL.h"

// The #define statement PSYSDIV in PLL.h
// initializes the PLL to the desired frequency.

// bus frequency is 480MHz/(PSYSDIV+1) = 480MHz/(3+1) = 120 MHz
// IMPORTANT: See Step 6) of PLL_Init().  If you change something, change 480 MHz.
// see the table at the end of this file


// configure the system to get its clock from the PLL
void PLL_Init(void){ unsigned long timeout;
  // 1) Once POR has completed, the PIOSC is acting as the system clock.  Just in case
  //    this function has been called previously, be sure that the system is not being
  //    clocked from the PLL while the PLL is being reconfigured.
  SYSCTL->RSCLKCFG &= ~SYSCTL_RSCLKCFG_USEPLL;
  // 2) Power up the MOSC by clearing the NOXTAL bit in the SYSCTL_MOSCCTL_R register.
  // 3) Since crystal mode is required, clear the PWRDN bit.  The datasheet says to do
  //    these two operations in a single write access to SYSCTL_MOSCCTL_R.
  SYSCTL->MOSCCTL &= ~(SYSCTL_MOSCCTL_NOXTAL|SYSCTL_MOSCCTL_PWRDN);
  //    Wait for the MOSCPUPRIS bit to be set in the SYSCTL_RIS_R register, indicating
  //    that MOSC crystal mode is ready.
  while((SYSCTL->RIS & SYSCTL_RIS_MOSCPUPRIS)==0){};
  // 4) Set both the OSCSRC and PLLSRC fields to 0x3 in the SYSCTL_RSCLKCFG_R register
  //    at offset 0x0B0.
  //    Temporarily get run/sleep clock from 25 MHz main oscillator.
  SYSCTL->RSCLKCFG = (SYSCTL->RSCLKCFG&~SYSCTL_RSCLKCFG_OSCSRC_M)+SYSCTL_RSCLKCFG_OSCSRC_MOSC;
  //    PLL clock from main oscillator.
  SYSCTL->RSCLKCFG = (SYSCTL->RSCLKCFG&~SYSCTL_RSCLKCFG_PLLSRC_M)+SYSCTL_RSCLKCFG_PLLSRC_MOSC;
  // 5) If the application also requires the MOSC to be the deep-sleep clock source,
  //    then program the DSOSCSRC field in the SYSCTL_DSCLKCFG_R register to 0x3.
  //    Get deep-sleep clock from main oscillator (few examples use deep-sleep; optional).
  SYSCTL->DSCLKCFG = (SYSCTL->DSCLKCFG&~SYSCTL_DSCLKCFG_DSOSCSRC_M)+SYSCTL_DSCLKCFG_DSOSCSRC_MOSC;
  // 6) Write the SYSCTL_PLLFREQ0_R and SYSCTL_PLLFREQ1_R registers with the values of
  //    Q, N, MINT, and MFRAC to configure the desired VCO frequency setting.
  //    ************
  //    The datasheet implies that the VCO frequency can go as high as 25.575 GHz
  //    with MINT=1023 and a 25 MHz crystal.  This is clearly unreasonable.  For lack
  //    of a recommended VCO frequency, this program sets Q, N, and MINT for a VCO
  //    frequency of 480 MHz with MFRAC=0 to reduce jitter.  To run at a frequency
  //    that is not an integer divisor of 480 MHz, change this section.
  //    fVC0 = (fXTAL/(Q + 1)/(N + 1))*(MINT + (MFRAC/1,024))
  //    fVCO = 480,000,000 Hz (arbitrary, but presumably as small as needed)
#define FXTAL 25000000  // fixed, this crystal is soldered to the Connected Launchpad
#define Q            0
#define N            4  // chosen for reference frequency within 4 to 30 MHz
#define MINT        96  // 480,000,000 = (25,000,000/(0 + 1)/(4 + 1))*(96 + (0/1,024))
#define MFRAC        0  // zero to reduce jitter
  //    SysClk = fVCO / (PSYSDIV + 1)
#define SYSCLK (FXTAL/(Q+1)/(N+1))*(MINT+MFRAC/1024)/(PSYSDIV+1)
  SYSCTL->PLLFREQ0 = (SYSCTL->PLLFREQ0 & ~SYSCTL_PLLFREQ0_MFRAC_M)+(MFRAC<<SYSCTL_PLLFREQ0_MFRAC_S) |
                      (SYSCTL->PLLFREQ0 & ~SYSCTL_PLLFREQ0_MINT_M)+(MINT<<SYSCTL_PLLFREQ0_MINT_S);
  SYSCTL->PLLFREQ1 = (SYSCTL->PLLFREQ1 & ~SYSCTL_PLLFREQ1_Q_M)+(Q<<SYSCTL_PLLFREQ1_Q_S) |
                      (SYSCTL->PLLFREQ1 & ~SYSCTL_PLLFREQ1_N_M)+(N<<SYSCTL_PLLFREQ1_N_S);
  SYSCTL->PLLFREQ0 |= SYSCTL_PLLFREQ0_PLLPWR;       // turn on power to PLL
  SYSCTL->RSCLKCFG |= SYSCTL_RSCLKCFG_NEWFREQ;      // lock in register changes
  // 7) Write the SYSCTL_MEMTIM0_R register to correspond to the new clock setting.
  //    ************
  //    Set the timing parameters to the main Flash and EEPROM memories, which
  //    depend on the system clock frequency.  See Table 5-12 in datasheet.
  if(SYSCLK < 16000000){
    // FBCHT/EBCHT = 0, FBCE/EBCE = 0, FWS/EWS = 0
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x0<<22) + (0x0<<21) + (0x0<<16) + (0x0<<6) + (0x0<<5) + (0x0);
  } else if(SYSCLK == 16000000){
    // FBCHT/EBCHT = 0, FBCE/EBCE = 1, FWS/EWS = 0
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x0<<22) + (0x1<<21) + (0x0<<16) + (0x0<<6) + (0x1<<5) + (0x0);
  } else if(SYSCLK <= 40000000){
    // FBCHT/EBCHT = 2, FBCE/EBCE = 0, FWS/EWS = 1
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x2<<22) + (0x0<<21) + (0x1<<16) + (0x2<<6) + (0x0<<5) + (0x1);
  } else if(SYSCLK <= 60000000){
    // FBCHT/EBCHT = 3, FBCE/EBCE = 0, FWS/EWS = 2
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x3<<22) + (0x0<<21) + (0x2<<16) + (0x3<<6) + (0x0<<5) + (0x2);
  } else if(SYSCLK <= 80000000){
    // FBCHT/EBCHT = 4, FBCE/EBCE = 0, FWS/EWS = 3
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x4<<22) + (0x0<<21) + (0x3<<16) + (0x4<<6) + (0x0<<5) + (0x3);
  } else if(SYSCLK <= 100000000){
    // FBCHT/EBCHT = 5, FBCE/EBCE = 0, FWS/EWS = 4
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x5<<22) + (0x0<<21) + (0x4<<16) + (0x5<<6) + (0x0<<5) + (0x4);
  } else if(SYSCLK <= 120000000){
    // FBCHT/EBCHT = 6, FBCE/EBCE = 0, FWS/EWS = 5
      SYSCTL->MEMTIM0 = (SYSCTL->MEMTIM0 & ~0x03EF03EF) + (0x6<<22) + (0x0<<21) + (0x5<<16) + (0x6<<6) + (0x0<<5) + (0x5);
  } else{
    // A setting is invalid, and the PLL cannot clock the system faster than 120 MHz.
    // Skip the rest of the initialization, leaving the system clocked from the MOSC,
    // which is a 25 MHz crystal.
    return;
  }
  // 8) Wait for the SYSCTL_PLLSTAT_R register to indicate that the PLL has reached
  //    lock at the new operating point (or that a timeout period has passed and lock
  //    has failed, in which case an error condition exists and this sequence is
  //    abandoned and error processing is initiated).
  timeout = 0;
  while(((SYSCTL->PLLSTAT & SYSCTL_PLLSTAT_LOCK) == 0) && (timeout < 0xFFFF)){
    timeout = timeout + 1;
  }
  if(timeout == 0xFFFF){
    // The PLL never locked or is not powered.
    // Skip the rest of the initialization, leaving the system clocked from the MOSC,
    // which is a 25 MHz crystal.
    return;
  }
  // 9)Write the SYSCTL_RSCLKCFG_R register's PSYSDIV value, set the USEPLL bit to
  //   enabled, and set the MEMTIMU bit.
  SYSCTL->RSCLKCFG = (SYSCTL->RSCLKCFG&~SYSCTL_RSCLKCFG_PSYSDIV_M)+(PSYSDIV&SYSCTL_RSCLKCFG_PSYSDIV_M) |
                       SYSCTL_RSCLKCFG_MEMTIMU |
                       SYSCTL_RSCLKCFG_USEPLL;
}


/*
PSYSDIV  SysClk (Hz)
  3     120,000,000
  4      96,000,000
  5      80,000,000
  7      60,000,000
  9      48,000,000
 15      30,000,000
 19      24,000,000
 29      16,000,000
 39      12,000,000
 79       6,000,000
*/
