/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* ADC.c
*
* Functions and systems for ADC setup and usage.
*
******************************************************************************/
#include "ADC.h"

void ADC_Init(void){
        // 1) activate clock for Port B, D, and K
    SYSCTL->RCGCGPIO |= SYSCTL_RCGCGPIO_R3 | SYSCTL_RCGCGPIO_R1 | SYSCTL_RCGCGPIO_R9;
    // allow time for clocks to stabilize
    while ((SYSCTL->PRGPIO & SYSCTL_RCGCGPIO_R1) == 0);
    while ((SYSCTL->PRGPIO & SYSCTL_RCGCGPIO_R3) == 0);
    while ((SYSCTL->PRGPIO & SYSCTL_RCGCGPIO_R9) == 0);

    // PB4, PB5, PD2, PD5, PD6, PK0, PK1, PK2, PK3
    GPIOB->AFSEL |= BIT4 | BIT5;
    GPIOD->AFSEL |= BIT2 | BIT5 | BIT6;
    GPIOK->AFSEL |= BIT0 | BIT1 | BIT2 | BIT3;

    GPIOB->DEN &= ~(BIT4 | BIT5);
    GPIOD->DEN &= ~(BIT2 | BIT5 | BIT6);
    GPIOK->DEN &= ~(BIT0 | BIT1 | BIT2 | BIT3);


    GPIOB->AMSEL |= BIT4 | BIT5;
    GPIOD->AMSEL |= BIT2 | BIT5 | BIT6;
    GPIOK->AMSEL |= BIT0 | BIT1 | BIT2 | BIT3;
    // 5) activate clock for ADC0
    SYSCTL->RCGCADC |= SYSCTL_RCGCADC_R0;
    // allow time for clock to stabilize
    while((SYSCTL->PRADC & SYSCTL_PRADC_R0) == 0){};
    // 6) configure ADC clock source as PLL VCO / 15 = 32 MHz
    ADC0->CC = (( ADC0->CC & ~ADC_CC_CLKDIV_M ) + (14<<ADC_CC_CLKDIV_S)) |
    ((ADC0->CC & ~ADC_CC_CS_M)+ADC_CC_CS_SYSPLL);

    ADC0->SSPRI = BIT(13) | BIT(12) | BIT9 | BIT4; // 7) sequencer 0 is highest priority
    // 8) enable sample sequencer 0
    ADC0->ACTSS &= ~(ADC_ACTSS_ASEN0 | ADC_ACTSS_ASEN1);
    // 9) configure seq2 for software trigger (default)
    ADC0->EMUX = (ADC0->EMUX & ~(ADC_EMUX_EM0_M | ADC_EMUX_EM1_M)) | ADC_EMUX_EM0_PROCESSOR | ADC_EMUX_EM1_PROCESSOR;
    // 10) configure for no hardware oversampling (default)
    ADC0->SAC = (ADC0->SAC & ~ADC_SAC_AVG_M) | ADC_SAC_AVG_OFF;
    // 11) configure for internal reference (default)
    ADC0->CTL = (ADC0->CTL & ~ADC_CTL_VREF_M) | ADC_CTL_VREF_INTERNAL;
    // 12) configure for ADC results saved to FIFO (default)
    ADC0->SSOP0 = 0;
    ADC0->SSOP1 = 0;
    // 13) configure for 4 ADC clock period S&H (default)
    // 14) set channels
    ADC0->SSMUX0 = 10 | (11 << 4) | (0 << 8) | (1 << 12) | (2 << 16) | (3 << 20) | (6 << 24) | (13 << 28);

    // 14) set channels Channel AIN5 on PD6
    ADC0->SSMUX1 = 5;

    ADC0->SSEMUX1 = 0;
    ADC0->SSEMUX0 = BIT8 | BIT(12) | BIT(16) | BIT(20); // 15) SS0 1st and 2nd sample channels in range 0:15

    ADC0->SSCTL0 = BIT(30) | BIT(29); // 16) yes IE7 END7
    ADC0->SSCTL1 = BIT1 | BIT2;

    ADC0->IM &= ~(ADC_IM_MASK0 | ADC_IM_MASK1);     // 17) disable SS0 interrupts

    ADC0->ACTSS |= (ADC_ACTSS_ASEN0 | ADC_ACTSS_ASEN1);// 18) enable sample sequencer 2
}


/*
 * |-----------|
 * | 5 | 7 | 6 |
 * |-----------|
 * | 3 | 8 | 4 |
 * |-----------|
 * | 0 | 1 | 2 |
 * |-----------|
 * PANEL FACING DOWN
 */
void PollADC(uint16_t * data){
    uint16_t i;
    ADC0->PSSI = ADC_PSSI_SS0 | ADC_PSSI_SS1;             // 1) initiate SS0
    while((ADC0->RIS & ADC_PSSI_SS1) == 0 && (ADC0->RIS & ADC_PSSI_SS0) == 0){}   // 2) wait for conversion done
    for (i = 0; i < 8; i++)
        data[i] = ADC0->SSFIFO0 & 0xFFF; // 3) read results
    data[8] = ADC0->SSFIFO1 & 0xFFF;
    ADC0->ISC = ADC_ISC_IN0 | ADC_ISC_IN1;   // 4) acknowledge completion
}
