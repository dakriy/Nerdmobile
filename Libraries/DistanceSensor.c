/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* DistanceSensor.c
*
* Functions and support for the distance sensor
*
******************************************************************************/
#include "DistanceSensor.h"

void initDistanceSensor(void)
{
		char startupRes = 0;
    // Assume I2C is already initialized.

    // Initialize GPIO0 on the sensor
    // Enable Port C
    SYSCTL->RCGCGPIO |= SYSCTL_RCGCGPIO_R2;

    // Check if the peripheral access is enabled.
    while(!(SYSCTL->PRGPIO & SYSCTL_PRGPIO_R2));

    // Enable the GPIO pin for the reset on the distance sensor
    GPIOC->PUR &= ~(BIT7);
    GPIOC->PDR &= ~(BIT7);
    GPIOC->DEN |= BIT7;
    GPIOC->DIR |= BIT7;
    GPIOC->DATA &= ~BIT7;

    while (startupRes != 0x01)
    {
        // Set GPIO0 to 0
        GPIOC->DATA &= ~BIT7;
        WaitMs(1);
        GPIOC->DATA |= BIT7;
        WaitMs(1);
        readReg(0x0016, &startupRes);
        WaitUs(100);
    }

    clearBits(0x0016, BIT0);

    // apply tuning settings to device
    writeReg(0x0207, 0x01);
    writeReg(0x0208, 0x01);
    writeReg(0x0133, 0x01);
    writeReg(0x0096, 0x00);
    writeReg(0x0097, 0xFD);
    writeReg(0x00e3, 0x00);
    writeReg(0x00e4, 0x04);
    writeReg(0x00e5, 0x02);
    writeReg(0x00e6, 0x01);
    writeReg(0x00e7, 0x03);
    writeReg(0x00f5, 0x02);
    writeReg(0x00D9, 0x05);
    writeReg(0x00DB, 0xCE);
    writeReg(0x00DC, 0x03);
    writeReg(0x00DD, 0xF8);
    writeReg(0x009f, 0x00);
    writeReg(0x00a3, 0x3c);
    writeReg(0x00b7, 0x00);
    writeReg(0x00bb, 0x3c);
    writeReg(0x00b2, 0x09);
    writeReg(0x00ca, 0x09);
    writeReg(0x0198, 0x01);
    writeReg(0x01b0, 0x17);
    writeReg(0x01ad, 0x00);
    writeReg(0x00FF, 0x05);
    writeReg(0x0100, 0x05);
    writeReg(0x0199, 0x05);
    writeReg(0x0109, 0x07);
    writeReg(0x010a, 0x30);
    writeReg(0x003f, 0x46);
    writeReg(0x01a6, 0x1b);
    writeReg(0x01ac, 0x3e);
    writeReg(0x01a7, 0x1f);
    writeReg(0x0103, 0x01);
    writeReg(0x0030, 0x00);
    writeReg(0x001b, 0x0A);
    writeReg(0x003e, 0x0A);
    writeReg(0x0131, 0x04);
    writeReg(0x0011, 0x10);
    writeReg(0x0014, 0x24);
    writeReg(0x0031, 0xFF);
    writeReg(0x00d2, 0x01);
    writeReg(0x00f2, 0x01);
    // The device should now be ready to take measurements
}

int writeReg(uint16_t address, char byte)
{
    char toSend[3];
    toSend[0] = (address & 0xFF00) >> 8;
    toSend[1] = (address & 0x00FF);
    toSend[2] = byte;

    return I2C_SendBytes(DISTANCE_SENSOR_SLAVE_ADDRESS, toSend, 3);
}

int readReg(uint16_t address, char* byte)
{
    int retValWrite;
    int retValRead;
    char toSend[2];
    toSend[0] = (address & 0xFF00) >> 8;
    toSend[1] = (address & 0x00FF);

    retValWrite = I2C_SendBytes(DISTANCE_SENSOR_SLAVE_ADDRESS, toSend, 2);

    retValRead = I2C_ReceiveByte(DISTANCE_SENSOR_SLAVE_ADDRESS, byte);

    return retValWrite | retValRead;
}

int setBits(uint16_t address, int bit)
{
    char regPreVal;
		int retval2;
    int retval1 = readReg(address, &regPreVal);

    regPreVal |= bit;

    retval2 = writeReg(address, regPreVal);
    return retval1 | retval2;
}

int clearBits(uint16_t address, int bit)
{
    char regPreVal;
		int retval2;
    int retval1 = readReg(address, &regPreVal);

    regPreVal &= ~(bit);

    retval2 = writeReg(address, regPreVal);
		return retval1 | retval2;
}

char getRangeMeasurement(void)
{
    char dist, busy = 0;

    // The next writeReg is for interrupts which I am not doing currently.
    // writeReg(0x0014, 0x04);
    setBits(0x0018, BIT0);
    while((busy & (BIT0 | BIT1 | BIT2)) != 0x04)
        readReg(0x004F, &busy);
    if(readReg(0x0062, &dist))
    	return 255;
    setBits(0x0015, BIT0 | BIT1 | BIT2);
    return dist;
}

short getAmbientLight(void)
{
    short ambientLight = 0;
    char busy = 0, upperBits, lowerBits = 0;
    setBits(0x0014, BIT5);
    clearBits(0x0014, BIT3 | BIT4);

    clearBits(0x0040, BIT0);

    writeReg(0x0041, 0x31);

    setBits(0x0038, BIT0);

    while ((busy & (BIT5|BIT4|BIT3)) != BIT5)
        readReg(0x004F, &busy);

    readReg(0x0050, &upperBits);

    readReg(0x0051, &lowerBits);
    ambientLight = (upperBits << 8) | lowerBits;
    setBits(0x0015, BIT0 | BIT1 | BIT2);
    return ambientLight;
}
