/******************************************************************************
*
* CPTR480
* Final Project
* Seth Ballance, JD Priddy
* 6/13/18
*
* Serial.c
*
******************************************************************************/
#include "Serial.h"

//------------UART_Init------------
// Initialize UART0 for 115,200 baud rate (clock from 16 MHz PIOSC),
// 8 bit word length, no parity bits, one stop bit, FIFOs enabled,
// no interrupts
// Input: none
// Output: none
void Serial_Init(void){
                                        // activate clock for UART0
  SYSCTL->RCGCUART |= SYSCTL_RCGCUART_R0;
                                        // activate clock for Port A
  SYSCTL->RCGCGPIO |= SYSCTL_RCGCGPIO_R0;
                                        // allow time for clock to stabilize
  while((SYSCTL->PRUART&SYSCTL_PRUART_R0) == 0){};
  UART0->CTL &= ~UART_CTL_UARTEN;      // disable UART
  UART0->IBRD = 8;                     // IBRD = int(16,000,000 / (16 * 115,200)) = int(8.6806)
  UART0->FBRD = 44;                    // FBRD = round(0.6806 * 64) = 44
                                        // 8 bit word length (no parity bits, one stop bit, FIFOs)
  UART0->LCRH = (UART_LCRH_WLEN_8|UART_LCRH_FEN);
                                        // UART gets its clock from the alternate clock source as defined by SYSCTL_ALTCLKCFG_R
  UART0->CC = (UART0->CC&~UART_CC_CS_M)+UART_CC_CS_PIOSC;
                                        // the alternate clock source is the PIOSC (default)
  SYSCTL->ALTCLKCFG = (SYSCTL->ALTCLKCFG&~SYSCTL_ALTCLKCFG_ALTCLK_M)+SYSCTL_ALTCLKCFG_ALTCLK_PIOSC;
  UART0->CTL &= ~UART_CTL_HSE;         // high-speed disable; divide clock by 16 rather than 8 (default)
  UART0->CTL |= UART_CTL_UARTEN;       // enable UART
                                        // allow time for clock to stabilize
  while((SYSCTL->PRGPIO&SYSCTL_PRGPIO_R0) == 0){};
  GPIOA->AFSEL |= 0x03;           // enable alt funct on PA1-0
  GPIOA->DEN |= 0x03;             // enable digital I/O on PA1-0
                                        // configure PA1-0 as UART
  GPIOA->PCTL = (GPIOA->PCTL&0xFFFFFF00)+0x00000011;
  GPIOA->AMSEL &= ~0x03;          // disable analog functionality on PA
}

//------------Serial_InChar------------
// Wait for new serial port input
// Input: none
// Output: ASCII code for key typed
char Serial_InChar(void){
  while((UART0->FR&UART_FR_RXFE) != 0);
  return((uint8_t)(UART0->DR&0xFF));
}

//------------Serial_OutChar------------
// Output 8-bit to serial port
// Input: letter is an 8-bit ASCII character to be transferred
// Output: none
void Serial_OutChar(char data){
  while((UART0->FR&UART_FR_TXFF) != 0);
  UART0->DR = data;
}

//------------Serial_OutString------------
// Output String (NULL termination)
// Input: pointer to a NULL-terminated string to be transferred
// Output: none
void Serial_OutString(char *pt){
  while(*pt){
    Serial_OutChar(*pt);
    pt++;
  }
}

//------------Serial_InUDec------------
// InUDec accepts ASCII input in unsigned decimal format
//     and converts to a 32-bit unsigned number
//     valid range is 0 to 4294967295 (2^32-1)
// Input: none
// Output: 32-bit unsigned number
// If you enter a number above 4294967295, it will return an incorrect value
// Backspace will remove last digit typed
uint32_t Serial_InUDec(void){
uint32_t number=0, length=0;
char character;
  character = Serial_InChar();
  while(character != CR){ // accepts until <enter> is typed
// The next line checks that the input is a digit, 0-9.
// If the character is not 0-9, it is ignored and not echoed
    if((character>='0') && (character<='9')) {
      number = 10*number+(character-'0');   // this line overflows if above 4294967295
      length++;
      Serial_OutChar(character);
    }
// If the input is a backspace, then the return number is
// changed and a backspace is outputted to the screen
    else if((character==BS) && length){
      number /= 10;
      length--;
      Serial_OutChar(character);
    }
    character = Serial_InChar();
  }
  return number;
}

//-----------------------Serial_OutUDec-----------------------
// Output a 32-bit number in unsigned decimal format
// Input: 32-bit number to be transferred
// Output: none
// Variable format 1-10 digits with no space before or after
void Serial_OutUDec(uint32_t n){
// This function uses recursion to convert decimal number
//   of unspecified length as an ASCII string
  if(n >= 10){
    Serial_OutUDec(n/10);
    n = n%10;
  }
  Serial_OutChar(n+'0'); /* n is between 0 and 9 */
}

//---------------------Serial_InUHex----------------------------------------
// Accepts ASCII input in unsigned hexadecimal (base 16) format
// Input: none
// Output: 32-bit unsigned number
// No '$' or '0x' need be entered, just the 1 to 8 hex digits
// It will convert lower case a-f to uppercase A-F
//     and converts to a 16 bit unsigned number
//     value range is 0 to FFFFFFFF
// If you enter a number above FFFFFFFF, it will return an incorrect value
// Backspace will remove last digit typed
uint32_t Serial_InUHex(void){
uint32_t number=0, digit, length=0;
char character;
  character = Serial_InChar();
  while(character != CR){
    digit = 0x10; // assume bad
    if((character>='0') && (character<='9')){
      digit = character-'0';
    }
    else if((character>='A') && (character<='F')){
      digit = (character-'A')+0xA;
    }
    else if((character>='a') && (character<='f')){
      digit = (character-'a')+0xA;
    }
// If the character is not 0-9 or A-F, it is ignored and not echoed
    if(digit <= 0xF){
      number = number*0x10+digit;
      length++;
      Serial_OutChar(character);
    }
// Backspace outputted and return value changed if a backspace is inputted
    else if((character==BS) && length){
      number /= 0x10;
      length--;
      Serial_OutChar(character);
    }
    character = Serial_InChar();
  }
  return number;
}

//--------------------------Serial_OutUHex----------------------------
// Output a 32-bit number in unsigned hexadecimal format
// Input: 32-bit number to be transferred
// Output: none
// Variable format 1 to 8 digits with no space before or after
void Serial_OutUHex(uint32_t number){
// This function uses recursion to convert the number of
//   unspecified length as an ASCII string
  if(number >= 0x10){
    Serial_OutUHex(number/0x10);
    Serial_OutUHex(number%0x10);
  }
  else{
    if(number < 0xA){
      Serial_OutChar(number+'0');
     }
    else{
      Serial_OutChar((number-0x0A)+'A');
    }
  }
}

//------------Serial_InString------------
// Accepts ASCII characters from the serial port
//    and adds them to a string until <enter> is typed
//    or until max length of the string is reached.
// It echoes each character as it is inputted.
// If a backspace is inputted, the string is modified
//    and the backspace is echoed
// terminates the string with a null character
// uses busy-waiting synchronization on RDRF
// Input: pointer to empty buffer, size of buffer
// Output: Null terminated string
// -- Modified by Agustinus Darmawan + Mingjie Qiu --
void Serial_InString(char *bufPt, uint16_t max) {
uint16_t length=0;
char character;
  character = Serial_InChar();
  while(character != CR){
    if(character == BS){
      if(length){
        bufPt--;
        length--;
        Serial_OutChar(BS);
      }
    }
    else if(length < max){
      *bufPt = character;
      bufPt++;
      length++;
      Serial_OutChar(character);
    }
    character = Serial_InChar();
  }
  *bufPt = 0;
}


//---------------------OutCRLF---------------------
// Output a CR,LF to UART to go to a new line
// Input: none
// Output: none
void OutCRLF(void){
	Serial_OutChar(0xFE);
  Serial_OutChar(CR);
	Serial_OutChar(0xFE);
  Serial_OutChar(LF);
}
